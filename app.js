// Load express, web framework for node
// http://expressjs.com/
var express = require('express');
var app = express();

// Load mongoose, mongodb object modeling for nodejs
// http://mongoosejs.com/
var mongoose = require('mongoose');

// Connect to MongoHQ db via connection url we get from heroku
mongoose.connect(process.env.MONGOHQ_URL);

// Hook callbacks to the connection attempt, maintain a status variable with connection status
var db_connect = false;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error: '));
db.once('open', function callback() {
	console.log('connected to db success');
	db_connect = true;
});

// Define a Person model with mongoose, it has a Name, a Position, and a Status, all are strings
var Person = mongoose.model('Person', { name: String, position: String, status: String });


function checkDB(callback) {
	// If we're connected to the DB, go ahead; else fail out with a status msg
	if(db_connect) {
		callback();
	} else {
		res.send(500, {status:'not connected to db'});
	}
}
// Read function for users (/users/Doug)
app.get('/users/:name', function(req, res) {

	// If we're connected to the DB, go ahead; else fail out with a status msg
	checkDB(function() {
		var msg = {};

		// Attempt to find one person in the DB with the name we get from the request parameters
		Person.findOne({ name: req.params.name }, function(err, person) {
			// If we get an error, log it
			if (err) console.log(err);

			// If we found a person, send the response with the found person
			// Else return a 404 with a descriptive message
			if(person) {
				msg.person = person;
				res.send(msg);
			} else {
				msg.status = "doesn't exist";
				res.send(404, msg);
			}
		});
	});
});

app.get('/users', function(req, res) {
	checkDB(function(){
		var msg;

		Person.find({}, function(err, docs){
			if(err) console.log(err);

			if(docs) {
				msg = docs;
				res.send(msg);
			} else {
				msg.status = "there are no users";
				res.send(msg);
			};
		});
	});
});


//POST function
app.post('/users', function(req, res) {
	
	//DB connection check
	checkDB(function() {

		//Check if arguments are passed, if yes read these argument
		if(req.query.name && req.query.name.length > 0){

			//Check to see if entry already exists using Person.count
			Person.count({ name: req.query.name }, function(err, count) {
				if(err) console.log(err);

				//If count returns a value greater than 1, then the person already exists.
				if(count > 0) {

					//Person already exists
					res.send(409, {status: "user with that name already exists"});
				} else {

					//If person doesn't exist, then make an object.
					var args = { name: req.query.name };

					//Check to see if there's also a position, if yes then add it to the ARGs object.
					if(req.query.position && req.query.position.length > 0) args.position = req.query.position;
					
					//Check to see if there's also a status, if yes then add it to the ARGs object.
					if(req.query.status && req.query.status.length > 0) args.status = req.query.status;
					
					//Create the Person from args object.
					Person.create(args, function(err, person) {
						if(err) {

							//If there's an error, error.
							console.log(err);
							res.send(500, {status:'there was an error creating the user'});
						} else if(person) {

							//If there's no errors, create person.
							res.send(201, {status:'user created', person: person});
						} else {

							//If there's no errors, but the person isn't made, WHO KNOWS.
							res.send(500, {status:'honestly not sure what happened'});
						}
					});
				}
			});
		} else {

			//If there's no arguments, please send arguments.
			res.send(400, {status: 'user requires a name'});
		};
	});
});


//PATCH function
app.patch('/users/:name', function(req, res) {
	
	//DB connection check
	checkDB(function() {

		var msg = {};

		// Attempt to find one person in the DB with the name we get from the request parameters
		Person.findOne({ name: req.params.name }, function(err, person) {
			// If we get an error, log it
			if (err) console.log(err);

			// If we found a person, send the response with the found person
			// Else return a 404 with a descriptive message
			if(person) {
				
				var args = {};

				//Check to see if there's also a position, if yes then add it to the ARGs object.
				if(req.query.position && req.query.position.length > 0) args.position = req.query.position;
				
				//Check to see if there's also a status, if yes then add it to the ARGs object.
				if(req.query.status && req.query.status.length > 0) args.status = req.query.status;

				//Update the Person from args object.
				Person.update({ name: req.params.name }, args, function(err) {
					if(err) {

						//If there's an error, error.
						console.log(err);
						res.send(500, {status:'there was an error updating the user'});
					} else if(person) {

						//If there's no errors, update person.
						res.send(201, {status:'user updated', person: person});
					}
				});
			} else {
				msg.status = "doesn't exist";
				res.send(404, msg);
			}
		});
	});
});



//DELETE function
app.delete('/users/:name', function(req, res) {
	
	//DB connection check
	checkDB(function() {

		var msg = {};

		// Attempt to find one person in the DB with the name we get from the request parameters
		Person.findOne({ name: req.params.name }, function(err, person) {
			// If we get an error, log it
			if (err) console.log(err);

			// If we found a person, send the response with the found person
			// Else return a 404 with a descriptive message
			if(person) {
				//Delete the Person from args object.
				Person.remove(person, function(err) {
					if(err) {
						//If there's an error, error.
						console.log(err);
						res.send(500, {status:'there was an error deleting the user'});
					} else {

						//If there's no errors, but the person isn't made, WHO KNOWS.
						res.send(201, {status:'user deleted'});
					}
				});
			} else {
				msg.status = "doesn't exist";
				res.send(404, msg);
			}
		});
	});
});


// Tell express to listen for requests on the port we get from heroku
app.listen(process.env.PORT);